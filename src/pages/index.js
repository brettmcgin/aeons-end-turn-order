import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Deck from "../components/deck"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Deck includeAssault={true} />
  </Layout>
)

export default IndexPage
