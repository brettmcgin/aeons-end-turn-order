import React, { useState, Fragment, useEffect } from "react"
import PropTypes from "prop-types"
import Card from "./card"

import Nemesis from "./nemesis"
import AnholdLegolas from "./anhold_legolas"
import RegarthKratos from "./regath_kratos"
import Assault from "./assault"

const Deck = props => {
  const fronts = [
    { front: <Nemesis />, isShowing: true },
    { front: <AnholdLegolas />, isShowing: true },
    { front: <AnholdLegolas />, isShowing: true },
    { front: <RegarthKratos />, isShowing: true },
    { front: <RegarthKratos />, isShowing: true },
  ]

  if(props.includeAssault) {
    fronts.push({ front: <Assault />, isShowing: true });
  } else {
    fronts.push({ front: <Nemesis />, isShowing: true });
  }

  const [count, setCount] = useState(fronts.length)
  const [deckFronts, setDeckFronts] = useState(fronts)

  useEffect(() => {
    if (count > fronts.length) {
      setCount(0)
      let shuffled = shuffle(deckFronts)
      shuffled.forEach(s => (s.isShowing = false))
      setDeckFronts(shuffled)
    } else {
      deckFronts.forEach((f, i) => {
        f.isShowing = count >= i
      })

      setDeckFronts(deckFronts)
    }
  })

  return (
    <div
      style={{
        display: `flex`,
        flexDirection: `row`,
        height: `100vh`,
        width: `${95 / fronts.length}vw`,
      }}
      onClick={() => setCount(count + 1)}
    >
      <Fragment>
        {deckFronts.map(f => (
          <Card isShowing={f.isShowing} front={f.front} />
        ))}
      </Fragment>
    </div>
  )
}

function shuffle(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }

  return array
}

Deck.defaultProps = {
  includeAssault: false,
}

Deck.propTypes = {
  includeAssault: PropTypes.bool,
}

export default Deck
