import React from "react"
import nemesis from "../images/nemesis.jpg"

const Nemesis = () => {
  return <img src={nemesis} alt="Logo" style={{margin: 0}} />
}

export default Nemesis
