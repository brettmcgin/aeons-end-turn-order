import React from "react"
import assault from "../images/assault.jpg"

const Assault = () => {
  return <img src={assault} alt="Logo" style={{margin: 0}} />
}

export default Assault
