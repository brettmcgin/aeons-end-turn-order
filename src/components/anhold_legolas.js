import React from "react"

import anholdLegolas from "../images/anhold_legolas.jpg"

const AnholdLegolas = () => {
  return <img src={anholdLegolas} alt="Logo" style={{margin: 0}} />
}

export default AnholdLegolas
