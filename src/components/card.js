import React from "react"
import PropTypes from "prop-types"

import Back from "./back"

const Card = ({ isShowing, front }) => {
  if (isShowing === true) {
    return <>{front}</>
  }

  return (
    <>
      <Back />
    </>
  )
}

Card.defaultProps = {
  isShowing: false,
}

Card.propTypes = {
  isShowing: PropTypes.bool,
  front: PropTypes.element,
}

export default Card
