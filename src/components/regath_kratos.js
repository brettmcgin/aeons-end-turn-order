import React from "react"
import regarthKratos from "../images/regarth_kratos.jpg"

const RegathKratos = () => {
  return <img src={regarthKratos} alt="Logo" style={{margin: 0}} />
}

export default RegathKratos
