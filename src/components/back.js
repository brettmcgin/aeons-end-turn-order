import React from "react"
import back from "../images/back.jpg"

const Back = () => {
  return <img src={back} alt="Logo" style={{margin: 0}} />
}

export default Back
