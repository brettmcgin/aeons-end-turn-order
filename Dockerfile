FROM node:14.3

RUN apt-get update && \
    apt-get install -y libgl1-mesa-dev

RUN npm install -g gatsby-cli

WORKDIR /app

COPY package*.json ./

RUN npm i

COPY . .

RUN gatsby build

CMD gatsby serve
